
public class Hello
{
    public void foo()
    {
        if (isHashCodeEven()) {
            System.out.println("foo if branch");
        }
        else {
            System.out.println("foo else branch");
        }
    }



    public void bar()
    {
        if (isHashCodeEven()) {
            System.out.println("bar if branch");
        }
        else {
            System.out.println("bar else branch");
        }
    }



    private boolean isHashCodeEven()
    {
        return getClass().getSimpleName().hashCode() % 2 == 0;
    }
}
