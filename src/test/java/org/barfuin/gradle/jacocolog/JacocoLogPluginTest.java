package org.barfuin.gradle.jacocolog;

import org.gradle.api.Project;
import org.gradle.testfixtures.ProjectBuilder;
import org.gradle.testing.jacoco.tasks.JacocoReport;
import org.junit.Assert;
import org.junit.Test;


public class JacocoLogPluginTest
{
    @Test
    public void testInvalidReportTaskName()
    {
        Project project = ProjectBuilder.builder().build();
        project.getTasks().register("invalidName", JacocoReport.class);

        project.getPlugins().apply(JacocoLogPlugin.PLUGIN_ID);

        Assert.assertNotNull(project.getTasks().findByName("jacocoLogCoverage1"));
    }
}
