/*
 * Copyright 2019 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import javax.annotation.Nonnull;
import javax.xml.parsers.ParserConfigurationException;

import groovy.lang.Closure;
import groovy.util.Node;
import groovy.util.NodeList;
import groovy.util.XmlParser;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.plugins.JavaBasePlugin;
import org.gradle.api.tasks.Nested;
import org.gradle.api.tasks.TaskAction;
import org.gradle.testing.jacoco.tasks.JacocoReport;
import org.gradle.util.ConfigureUtil;
import org.xml.sax.SAXException;


/**
 * The Gradle task that logs the coverage information is added by the plugin for each JacocoReport task.
 */
public class LogCoverageTask
    extends DefaultTask
{
    private static final DecimalFormatSymbols NUMBER_FORMAT_LOCALE = new DecimalFormatSymbols(Locale.ENGLISH);

    private JacocoReport reportTask = null;

    private JacocoCounters counters = new JacocoCounters();



    public LogCoverageTask()
    {
        setGroup(JavaBasePlugin.VERIFICATION_GROUP);
    }



    @Nested
    public JacocoCounters getCounters()
    {
        return counters;
    }



    /**
     * Configures the counters to be logged by this task.
     *
     * @param pClosure a configuration closure for a JacocoCounters object
     * @return the configured object
     */
    public JacocoCounters counters(final Closure<?> pClosure)
    {
        return counters(ConfigureUtil.configureUsing(pClosure));
    }



    /**
     * Configures the counters to be logged by this task.
     *
     * @param pConfigureAction a configuration action for a JacocoCounters object
     * @return the configured object
     */
    public JacocoCounters counters(final Action<? super JacocoCounters> pConfigureAction)
    {
        pConfigureAction.execute(counters);
        return counters;
    }



    public void setReportTask(final JacocoReport pReportTask)
    {
        if (getLogger().isInfoEnabled()) {
            getLogger().info(
                "Configuring log task '" + getName() + "' for report task '" + pReportTask.getName() + "'");
        }
        reportTask = pReportTask;
        setDescription("Logs JaCoCo test coverage from report task '" + pReportTask.getName() + "'.");
        dependsOn(pReportTask);
        pReportTask.finalizedBy(this);
        enableXmlReport(pReportTask);
    }



    private void enableXmlReport(final JacocoReport pReportTask)
    {
        pReportTask.configure(new Closure<Void>(this)
        {
            @Override
            @SuppressWarnings("MethodDoesntCallSuperMethod")
            public Void call()
            {
                ((JacocoReport) getDelegate()).getReports().getXml().setEnabled(true);
                return null;
            }
        });
    }



    @TaskAction
    public void logCoverage()
    {
        if (reportTask == null) {
            throw new GradleException("Bug: Report task not set for task '" + getName() + "'");
        }

        if (!reportTask.getReports().getXml().isEnabled()) {
            getLogger().error("ERROR: XML report not enabled for task '" + reportTask.getName()
                + "'. No coverage can be logged.");
            return;
        }

        File reportXmlFile = reportTask.getReports().getXml().getDestination();
        //noinspection ConstantConditions
        if (reportXmlFile != null) {
            final NodeList counters = readCounters(reportXmlFile);

            getLogger().lifecycle("Test Coverage:");
            for (JacocoCounterType type : JacocoCounterType.values()) {
                if (getCounters().isCounterEnabled(type)) {
                    logCoverage(counters, type);
                }
            }
        }
    }



    @Nonnull
    private NodeList readCounters(@Nonnull final File pReportXmlFile)
    {
        try {
            XmlParser parser = new XmlParser(false, true, true);
            parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            Node root = parser.parse(pReportXmlFile);
            NodeList counters = (NodeList) root.get("counter");
            if (counters == null || counters.isEmpty()) {
                throw new GradleException("No counters found in JaCoCo XML report: " + pReportXmlFile);
            }
            return counters;
        }
        catch (FileNotFoundException e) {
            throw new GradleException("JaCoCo XML report not found: " + pReportXmlFile, e);
        }
        catch (ParserConfigurationException | SAXException | IOException | RuntimeException e) {
            throw new GradleException("Failed to parse JaCoCo XML report: " + pReportXmlFile, e);
        }
    }



    private void logCoverage(@Nonnull final NodeList pCounters, @Nonnull final JacocoCounterType pType)
    {
        Node counter = findCounter(pCounters, pType);
        double cov = getCoverageFromCounter(counter);

        DecimalFormat df = new DecimalFormat("#.#", NUMBER_FORMAT_LOCALE);
        getLogger().lifecycle("    - " + pType.toString() + " Coverage: " + df.format(cov) + "%");
    }



    @Nonnull
    private Node findCounter(@Nonnull final NodeList pCounters, @Nonnull final JacocoCounterType pType)
    {
        Node result = null;
        for (Object nodeObj : pCounters) {
            if (nodeObj instanceof Node) {
                Node node = (Node) nodeObj;
                if (pType.getType().equals(node.get("@type"))) {
                    result = node;
                    break;
                }
            }
        }
        if (result == null) {
            throw new GradleException("Counter of type " + pType.getType() + " not found in JaCoCo XML report");
        }
        return result;
    }



    private double getCoverageFromCounter(@Nonnull final Node pCounter)
    {
        int missed = Integer.parseInt((String) pCounter.get("@missed"));
        int covered = Integer.parseInt((String) pCounter.get("@covered"));
        return ((double) covered / (missed + covered)) * 100;
    }
}
