# gradle-jacoco-log

Gradle plugin that logs test coverage calculated by the
[JaCoCo Gradle Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html) to the Gradle build log.

Kinda surprising that Gradle doesn't do this by default, but that's how it is.

## How to use

*gradle-jacoco-log* is published to the Gradle plugin portal, so you can just use it in your build like so:

**build.gradle:**

```groovy
plugins {
    id 'org.barfuin.gradle.jacocolog' version '1.0.1'
}
```

That's it! Every time a
[JacocoReport](https://docs.gradle.org/current/dsl/org.gradle.testing.jacoco.tasks.JacocoReport.html) task runs
(e.g. `jacocoTestReport`), the test coverage will be logged like this:

```
Test Coverage:
    - Class Coverage: 100%
    - Method Coverage: 100%
    - Branch Coverage: 81.2%
    - Line Coverage: 97.8%
    - Instruction Coverage: 95.3%
    - Complexity Coverage: 90.2%
```

## Details

Next are some more details in case you have a complex build, or want to do advanced stuff.

### Task Naming Convention

The Gradle Jacoco Plugin implies a
[naming convention](https://github.com/gradle/gradle/blob/v6.0.1/subprojects/jacoco/src/main/java/org/gradle/testing/jacoco/plugins/JacocoPlugin.java#L238)
for its report tasks, which is "jacoco*TestTask*Report". In this way, the relationship between tasks, and the task
names are indicated. Two examples:

| Test task name | Report task name   | Log task name           |
|:---------------|:-------------------|:------------------------|
| test           | jacocoTestReport   | jacocoLogTestCoverage   |
| foobar         | jacocoFoobarReport | jacocoLogFoobarCoverage |

So, if you define your own JacocoReport tasks in your build, you should follow this naming convention. If you don't,
we'll improvise by calling our log tasks `jacocoLogCoverage1` and so on, with a number postfix.

All this naming stuff only concerns you if you have custom JacocoReport tasks in your build.


### Disabling the logging

In order to disable coverage logging for one particular JacocoReport task, you can simply disable the corresponding
log task (log task name according to naming convention above):

```groovy
jacocoLogFoobarCoverage {   // <-- this is the name of the log task
    enabled = false
}
```


### Configuration

It can be configured which counters are shown in the log. By default, all counters are shown, but you can hide
individual counters like so:

```groovy
jacocoLogTestCoverage {
    counters {
        showComplexityCoverage = false
        showClassCoverage = false
        showLineCoverage = false
    }
}
```

There are also convenience methods for all counters such as `showLineCoverageOnly()`, which set one flag and disable
all others. They can be combined.


### Integration with GitLab CI

GitLab CI can [parse the coverage
information](https://docs.gitlab.com/ce/user/project/pipelines/settings.html#test-coverage-parsing) from the log file.
It can only deal with a single value, so you must choose one of the counters. Line or instruction coverage are popular
for that.

In your pipeline definition, add a `coverage` entry, with `Instruction` replaced with whatever your counter is
([example](https://gitlab.com/barfuin/gradle-jacoco-log/blob/v1.0.1/.gitlab-ci.yml#L20)).

**.gitlab-ci.yml:**
```yaml
coverage: '/    - Instruction Coverage: ([0-9.]+)%/'
```
Another alternative is to enter the regex in the [GitLab CI
settings](https://gitlab.com/barfuin/gradle-jacoco-log/-/settings/ci_cd), but the .gitlab-ci.yml setting takes
precedence and is recommended.


## Development

In order to develop *gradle-jacoco-log* and build it on your local machine, you need:

- Java 8 JDK ([download](https://adoptopenjdk.net/releases.html?variant=openjdk8))
- Gradle, but we use the Gradle Wrapper, so there is nothing to install for you

The project is IDE agnostic. Just use your favorite IDE and make sure none of its control files get checked in.


## Status

*gradle-jacoco-log* is feature complete and stable. It is ready for enterprise deployment.


## License

*gradle-jacoco-log* is free software under the terms of the Apache License, Version 2.0.
Details in the [LICENSE](LICENSE) file.


## Contributing

Contributions of all kinds are very welcome! If you are going to spend a lot of time on your contribution, it may
make sense to raise an issue first and discuss your contribution. Thank you!
